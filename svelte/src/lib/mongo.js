import { MongoClient, ObjectId } from 'mongodb'
import { RECIPES_MONGO_USERNAME, RECIPES_MONGO_PASSWORD, RECIPES_MONGO_URI} from "$env/static/private"

const client = new MongoClient(`mongodb+srv://${RECIPES_MONGO_USERNAME}:${RECIPES_MONGO_PASSWORD}@${RECIPES_MONGO_URI}/?retryWrites=true&w=majority`)
await client.connect()

export default client