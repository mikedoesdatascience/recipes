import { json } from '@sveltejs/kit'
import { MongoClient, ObjectId } from 'mongodb'
import client from "$lib/mongo.js";

export async function load({ params }) {
  let recipe = await client.db('recipes').collection('recipes').findOne({'_id': new ObjectId(params._id)})
  return {recipe: JSON.parse(JSON.stringify(recipe))}
}