import client from "$lib/mongo.js";

export async function load({ params }) {
  let recipes = await client.db('recipes').collection('recipes').find({}).limit(10).toArray()
  console.log(JSON.parse(JSON.stringify(recipes)))
  return {'recipes': JSON.parse(JSON.stringify(recipes))}
}