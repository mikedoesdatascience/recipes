import { ObjectId } from 'mongodb'
import client from '$lib/mongo.js'

export async function GET({ url }) {
    let _id = url.searchParams.get('_id') ?? ''
    let recipe = await client.db('recipes').collection('recipes').findOne({'_id': new ObjectId(_id)})
    return JSON.parse(JSON.stringify(recipe))
}

export async function POST({ request }) {
    let recipe = await request.json()
    let result = await client.db('recipes').collection('recipes').insertOne(recipe)
    return result
}