import SvelteKitAuth from "@auth/sveltekit"
import Google from "@auth/core/providers/google"
import { RECIPES_GOOGLE_CLIENT_ID, RECIPES_GOOGLE_CLIENT_SECRET, RECIPES_SECRET } from "$env/static/private"

export const handle = SvelteKitAuth({
    providers: [Google({ clientId: RECIPES_GOOGLE_CLIENT_ID, clientSecret: RECIPES_GOOGLE_CLIENT_SECRET })],
    secret: RECIPES_SECRET,
  })